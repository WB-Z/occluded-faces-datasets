***********************************************************
***Data Label Format***
The format of the provided data set label is .cvs format, the first column of each row represents the file name of the image, and the subsequent columns represent the corresponding ID. Such as "367_0007.jgp, 16"

***Output file format***
At the time of testing, given a file in .csv format, the first column of each line is the file name of the face image to be recognized.
The output file format is the same as the label of the dataset, and is saved in .csv format. The first column of each row represents the file name of the image to be retrieved, and the following column represents the ID of the face (ie, the ID returned by the recognition) Same as the ID in ground-truth, where the ID in the gallery is not 0)
Such as
"
0007.jgp, 16
0008.jgp, 0
0009.jgp, 18
"

***Document Description***

Train training set
Train.csv training set label

-test_a
  |-probe Probe image set to be recognized
  |-gallery gallery
Test_a_probe.csv List of file names of probes to be identified
Test_a_gallery test set gallery tag

Note:
The ID of the training set and the ID of the test set are independent of each other.

***********************************************************